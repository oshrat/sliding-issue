import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';

import { Page2 } from '../page2/page2';


@Component({
  selector: 'page-page1',
  templateUrl: 'page1.html'
})
export class Page1 {
users: any;

  constructor(public navCtrl: NavController) {
    this.users = [{ name: "AAA"}, 
                    { name: "BBB"},
                    { name: "CCC"},
                    { name: "DDD"},
                    { name: "EEE"},
                    { name: "FFF"},
                    { name: "GGG"},
                    { name: "HHH"}];
  }

    userSelected(user) {
        var self = this;
        self.navCtrl.push(Page2, { "puser": user });
    }
}
